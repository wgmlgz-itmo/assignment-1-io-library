section .data

%define stdout 1
%define int_base 10
%define write 1
%define exit_call 60

section .text
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rdi, rax
    mov rax, exit_call
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax            ; Clear RAX (to store the length)
    .loop:
        cmp byte [rdi + rax], 0      ; Compare the current character with null terminator
        je .done                ; If it's the null terminator, we're done
        inc rax                 ; Increment the length
        jmp .loop               ; Repeat the loop
    .done:
        ret                     ; Return with the length in RAX

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    test rdi, rdi
    je .end
    push rdi
    call string_length
    mov rdx, rax ; size
    mov rdi, stdout ;discriptor
    mov rax, write; call
    pop rsi ; ptr
    syscall

    .end: ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1 ; size
    mov rdi, stdout ;discriptor
    mov rax, write; call
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r9
    mov rax, rdi
    mov r9, rsp ; r9 = current pos to write
    sub rsp, 32
    mov rcx, int_base ; setup base
    dec r9
    mov [r9], byte 0
    test rax, rax
    jne .nonzero
    dec r9
    mov [r9], byte '0'
    jmp .print
    .nonzero:
        test rax, rax
        je .print
        xor rdx, rdx
        div rcx
        add rdx, '0'
        dec r9
        mov [r9], dl
        jmp .nonzero
    .print:
        mov rdi, r9
        call print_string
        add rsp, 32
        pop r9
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .more
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .more:
      jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rbx
    .loop:
        mov al, [rdi]
        mov bl, [rsi]
        cmp al, bl  ; if chars not equal return
        jne .ne  
        test al, al  ; if a=0, then b=0, so equal
        je .eq
        inc rdi
        inc rsi
        jmp .loop
    .eq:
        pop rbx
        mov rax, 1
        ret
    .ne:
        pop rbx
        xor rax, rax
        ret 


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdx
    xor rax, rax 
    xor rdi, rdi
    push 0  
    mov rsi, rsp   
    mov rdx, 1 
    syscall
    pop rax
    pop rdx
    test rax, rax
    je .end_of_input
    ret
    .end_of_input:
        xor rax, rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12 ; fin ptr
    push r14
    push r15 ; fin ptr
    xor rdx, rdx
    mov r12, rdi  ; r8 - cur
    push rdi
    mov r14, rsi
    add r14, rdi ; r14 - fin
    xor r15, r15
    add r15, r14
    .loop:
        call read_char
        jne .sus
        cmp rax, ' '
        je .loop
        cmp rax, 0x9 ; for some reason doen't pass test if changes for '\t'
        je .loop
        cmp rax, 0xA
        je .loop
        jmp .sus
    .nofirst:
        call read_char
    .sus:
        cmp rax, ' '
        je .end
        cmp rax, 0x9
        je .end
        cmp rax, 0xA
        je .end
        mov [r12], rax
        inc r12
        cmp r14, r12
        je .fail
        xor r15, r15
        mov [r12], r15
        test rax, rax
        je .end
        inc rdx
        jmp .nofirst
    .fail:
        pop rax
        pop r15
        pop r14
        pop r12
        xor rax, rax
        xor rdx, rdx
        ret
    .end:
        pop rax
        pop r15
        pop r14
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor rax, rax
    xor rdx, rdx
    mov r10, int_base
    .loop:
        xor rbx, rbx
        mov bl, byte [rdi]
        cmp rbx, '0'
        jl .end
        cmp rbx, '9'
        jg .end
        inc rdx
        sub rbx, '0'
        push rdx
        mul r10
        add rax, rbx
        pop rdx
        inc rdi
        jmp .loop
    .end:
        pop rbx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte [rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi ; str
    push rsi ; buff
    call string_length
    pop rsi
    pop rdi
    cmp rax, rdx
    jl .loop_copy
    xor rax, rax
    ret
    .loop_copy:
        mov al, [rdi]
        mov [rsi], al
        test al, al
        je .fin
        inc rdi
        inc rsi
        jmp .loop_copy
    .fin:
    ret